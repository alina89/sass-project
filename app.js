var compile = require("./index");
var sass = require('node-sass');
var glob = require("glob");
var fs = require('fs');
var sassGraph = require('sass-graph');
var path = require('path');
var mkdirp = require('mkdirp');

var targetDirectoryRoot = "css files/";
var sourcePatterns = ['sass files/**/*.scss', 'C:/New folder/**/*.scss'];

function getRelativeTarget(filePath, targetDirectoryRoot) {
	var fullDirectoryName, relativePath, newPath;

	fullDirectoryName = path.dirname(filePath);
	relativePath = path.parse(fullDirectoryName);
	if (relativePath.root.length > 0) {
		relativePath.root = "/";
		relativePath.dir = relativePath.dir.substring(2);
	}
	relativePath = path.format(relativePath);
	newPath = path.join(targetDirectoryRoot, relativePath);
	newPath = path.normalize(newPath);
	//console.log(newPath);
	return newPath;
}

var callback_func = function callback(filesArray, patterns, directory) {

	//console.log("Total files to compile: " + filesArray.length);
	var filenames = [];
	var funcs = [];
	var tempPath;

	for (var j = 0; j < filesArray.length; j++) {
		var filename = filesArray[j].replace(/^.*[\\\/]/, '');
		filenames[j] = filename.replace("scss", "css");
	}

	function createfunc(i) {
		return function(result) {
			tempPath = getRelativeTarget(filesArray[i], targetDirectoryRoot);
			mkdirp.sync(tempPath);
			fs.writeFile(path.join(tempPath, filenames[i]), result.css, function(err) {
				if (err)
					console.log(err);
				//else
				//console.log("The file " + filenames[i] + " was saved!");
			});
		};
	}

	for (var i = 0; i < filenames.length; i++) {
		funcs[i] = createfunc(i);
	}

	for (var i = 0; i < filesArray.length; i++) {
		sass.render({
			file: filesArray[i],
			success: funcs[i]
				//console.log(result.css);
				//console.log(result.stats);
				//console.log(result.map);
				,

			error: function(error) {
				console.log("*************************************************************************************************");
				console.log(error.message);
				console.log("Error status: " + error.status);
				console.log("Error line: " + error.line);
				console.log("Error Column: " + error.column);
				console.log("*************************************************************************************************");
			}
		});
	}

}

compile.get(sourcePatterns, targetDirectoryRoot, callback_func);