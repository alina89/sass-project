var glob = require("glob");
var fs = require('fs');
var sassGraph = require('sass-graph');
var path = require('path'); 

function getShortestPath(sourcePattern) {
	var findIndex = sourcePattern.indexOf("*");
	var basePath = sourcePattern.substring(0, findIndex);
	return basePath;
}

function getRelativeTarget(filePath, targetDirectoryRoot) {
	var fullDirectoryName, relativePath, newPath;

	fullDirectoryName = path.dirname(filePath);
	relativePath = path.parse(fullDirectoryName);
	if (relativePath.root.length > 0) {
		relativePath.root = "/";
		relativePath.dir = relativePath.dir.substring(2);
	}
	relativePath = path.format(relativePath);
	newPath = path.join(targetDirectoryRoot, relativePath);
	newPath = path.normalize(newPath);
	//console.log(newPath);
	return newPath;
}

function findMissingCSS(filesArray, targetDirectoryRoot) {
	var cssFileOnly = [];
	var sassPathsmissingCSS = [];
	var sassPathsToCheck = [];
	var cssPathsToCheck = [];
	var filename, cssFilename, tempPath;

	for (var j = 0; j < filesArray.length; j++) {
		filename = path.basename(filesArray[j]);
		cssFilename = filename.substring(0, filename.lastIndexOf(".")) + ".css";
		cssFileOnly[j] = cssFilename;

		tempPath = getRelativeTarget(filesArray[j], targetDirectoryRoot);
		tempPath = path.join(tempPath, cssFileOnly[j]);
		if (!fs.existsSync(tempPath)) {
			//if(!(filename.charAt(0) == "_")) {
			sassPathsmissingCSS.push(filesArray[j]);
			//}
		} else {
			sassPathsToCheck.push(filesArray[j]);
			cssPathsToCheck.push(tempPath);
		}
	}
	return {
		missingCSS: sassPathsmissingCSS,
		sassSources: sassPathsToCheck,
		cssTargets: cssPathsToCheck
	};
}

function findNewerSCSS(SassArray, CssArray) {
	var newerSassMissingCss = [];
	var sassFile, cssFile;

	for (var i = 0; i < SassArray.length; i++) {
		sassFile = fs.statSync(SassArray[i]);
		cssFile = fs.statSync(CssArray[i]);

		if (sassFile.mtime.getTime() > cssFile.mtime.getTime()) {
			newerSassMissingCss.push(SassArray[i]);
		}
	}
	return newerSassMissingCss;
}

function findDepend(newSassArray, sourcePatterns, allFilesToCheck) {
	var allPathsAndImportedBy = [];
	var allDependencies = [];
	var basePath, graph, currentPath, currentAbsPath, imported, dependentPath, index, i;

	if (newSassArray.length == 0) {
		return [];
	}

	var absoluteToRelative = {};
	for (i = 0; i < allFilesToCheck.length; i++) {
		currentPath = allFilesToCheck[i];
		absoluteToRelative[path.resolve(currentPath)] = currentPath;
	}

	//find all files -> their "imported by" info and add to allPathsAndImportedBy
	for (i = 0; i < sourcePatterns.length; i++) {
		basePath = getShortestPath(sourcePatterns[i]);
		graph = sassGraph.parseDir(basePath).index;
		for (var key in graph) {
			allPathsAndImportedBy[key] = graph[key].importedBy;
		}
	}
	index = 0;
	//go over all the possible changed sass files
	for (var j = 0; j < newSassArray.length; j++) {
		newSassArray[j] = path.normalize(newSassArray[j]);
		currentPath = newSassArray[j]; //path.resolve(newSassArray[j]);
		currentAbsPath = path.resolve(currentPath);
		allDependencies.push(currentPath);
		imported = allPathsAndImportedBy[currentAbsPath];

		while (true) { //start the first round
			//added the first suspect, now add his children if they are not already in the list
			for (var k = 0; k < imported.length; k++) {
				dependentPath = path.normalize(imported[k]);
				dependentPath = absoluteToRelative[dependentPath];
				if (allDependencies.indexOf(dependentPath) == -1) {
					allDependencies.push(dependentPath);
				}
			}
			index++;
			if (index == allDependencies.length)
				break;
			else
				imported = allPathsAndImportedBy[path.resolve(allDependencies[index])];
		}
	}
	return allDependencies;
}

function checkSass(sourcePatterns, targetDirectoryRoot, callback) {
	var allFilesToCheck = [];
	var finalResult = [];
	var newerSassCompile = [];
	var counter = 0;
	var result, newerSass;

	for (var k = 0; k < sourcePatterns.length; k++) {
		glob(sourcePatterns[k], function(err, files) {
			allFilesToCheck = allFilesToCheck.concat(files);
			if (counter == (sourcePatterns.length - 1)) {
				//Check if a CSS file already exists and compile the ones that don't exist
				result = findMissingCSS(allFilesToCheck, targetDirectoryRoot);
				//From all the CSS files that exist, check if the SCSS file is newer then the CSS
				// if so -> compile, if not -> do nothing
				newerSass = findNewerSCSS(result.sassSources, result.cssTargets);
				//now find all the dependencies of the files and add them to a set
				newerSassCompile = findDepend(newerSass, sourcePatterns, allFilesToCheck);
				finalResult = (result.missingCSS).concat(newerSassCompile);
				console.log("Files to compile: " + finalResult);
				callback(finalResult, sourcePatterns, targetDirectoryRoot);
			}
			counter++;
		});
	}
}
module.exports.get = checkSass;